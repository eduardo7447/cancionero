#! /usr/bin/env python
import os, random, sys, math
import pygame
from pygame.locals import *
from configuracion import *
from extras import *
from funcionesVACIAS import *


#Funcion principal
pygame.init()
# Display
screen = pygame.display.set_mode((ANCHO, ALTO))
def ultimo_puntaje():                 #Funcion para mostrar el puntaje del ultimo juego
    ulti=open("ultimoPuntaje.txt","r")
    archivo=ulti.readline()
    return (archivo)
    ulti.close()

#Ventana del cancionero -----------------------------------------------------
def main():
        #musica de fondo
        intro=pygame.mixer.music.load('Musica/menu.mp3')    #Musica de fondo del menu
        pygame.mixer.music.play(-1)                       #Funcion para reproducir archivo mp3
        pygame.mixer.music.set_volume(0.12)             #Volumen de fondo del menu

        tiempo_atrazo= menu(ANCHO,ALTO)             #Tiempo transcurrido en el menu


        #Centrar la ventana y despues inicializar pygame
        os.environ["SDL_VIDEO_CENTERED"] = "1"
        pygame.init()


        #Preparar la ventana
        pygame.display.set_caption("Cancionero...")
        screen = pygame.display.set_mode((ANCHO, ALTO))
        fondo= pygame.image.load("ochobits.jpg")                #variable para cargar la imagen de fondo



        pygame.mixer.music.load('Musica/juego.mp3')     #musica de fondo del juego
        pygame.mixer.music.play(-1)                       #Funcion para reproducir archivo mp3
        pygame.mixer.music.set_volume(0.12)             ##Volumen de fondo del juego

        correcta=pygame.mixer.Sound('Musica/correcta.mp3')      #Efecto de sonido respuesta correcta
        incorrectaM=pygame.mixer.Sound('Musica/incorrecta.mp3')     #Efecto de sonido respuesta incorrecta
        racha=pygame.mixer.Sound('Musica/racha.mp3')       #Efecto de sonido racha de respuestas correctas



        #tiempo total del juego
        gameClock = pygame.time.Clock()
        totaltime = 0
        segundos = TIEMPO_MAX
        fps = FPS_inicial
        artistaYcancion=[]
        puntos = 0          #Acumulador de puntos inicial
        racha2= 0           #Acumulador de racha inicial
        palabraUsuario = ""
        letra=[]
        correctas=0         #Acumulador de respuestas correctas
        elegidos= []
        masDeUnaVuelta = False
        game_over=True    #Condicion para entrar a la pantalla final
        incorrecta=0      #Acumuladro de respuestas incorrectas

        #elige una cancion de todas las disponibles
        azar=random.randrange(1,N+1)
        elegidos.append(azar) #la agrega a la lista de los ya elegidos
        archivo= open(".\\letras\\"+str(azar)+".txt","r", encoding='utf-8') # abre el archivo elegido con unicode.


        #lectura del archivo y filtrado de caracteres especiales, la primer linea es el artista y cancion
        lectura(archivo, letra, artistaYcancion)

        #elige una linea al azar y su siguiente
        lista=seleccion(letra)

        ayuda = "Cancionero"
        dibujar(screen, palabraUsuario, lista, puntos,racha2, segundos, ayuda)


        while segundos > fps/1000 :
        # 1 frame cada 1/fps segundos
            gameClock.tick(fps)
            totaltime += gameClock.get_time()

            if True:
            	fps = 60

            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():

                #QUIT es apretar la X en la ventana
                if e.type == QUIT:
                    pygame.quit()
                    return()

                #Ver si fue apretada alguna tecla
                if e.type == KEYDOWN:
                    letraApretada = dameLetraApretada(e.key)
                    palabraUsuario += letraApretada
                    if e.key == K_BACKSPACE:
                        palabraUsuario = palabraUsuario[0:len(palabraUsuario)-1]
                    if e.key == K_RETURN:
                        #chequea si es correcta y suma o resta puntos
                        sumar=esCorrecta(palabraUsuario, artistaYcancion, correctas)
                        puntos=puntos+sumar

                        if sumar>0:                     #Contador de racha
                            racha2=racha2+1             #acumulador
                        else:
                            racha2=0                    #si respondo mal, el contador se reinicia a cero

                        if sumar>0:
                            correctas=correctas+1
                            correcta.play()         #efecto de sonido correcto
                            if correctas==3 or correctas==6 or correctas==9 or correctas==12 or correctas==15 or correctas==18:   #Racha
                                racha.play()            #Efecto de sonido de racha

                                racha.set_volume(0.3)   #Volumen de racha


                            correcta.set_volume (0.3)   #Volumen del sonido correcto

                        else:
                            incorrecta=incorrecta+1
                            correctas=0
                            incorrectaM.play() #efecto de sonido error
                            incorrectaM.set_volume(0.4) #Funcion para el volumen del sonido error

                        if len(elegidos)==N:
                                elegidos=[]
                                masDeUnaVuelta = True
                        azar=random.randrange(1,N+1)
                        while(azar in elegidos):
                            azar=random.randrange(1,N+1)

                        elegidos.append(azar)


                        if masDeUnaVuelta == True:
                            cancion_anterior=artistaEN(artistaYcancion)   #Luego de pasar por las 9 canciones agregamos una ayuda que se muestra por pantalla
                            ayuda = cancion_anterior[0]             # Muestra el artista de la cancion anterior


                        archivo= open(".\\letras\\"+str(azar)+".txt","r", encoding='utf-8')
                        palabraUsuario = ""
                        #lectura del archivo y filtrado de caracteres especiales
                        artistaYcancion=[]

                        letra = []
                        lectura(archivo, letra, artistaYcancion)

                        #elige una linea al azar y su siguiente
                        lista=seleccion(letra)

            segundos = (TIEMPO_MAX - pygame.time.get_ticks()/1000)+tiempo_atrazo+1      #Recupera los segundos perdidos en el menu, y los suma a los segundos del juego

            #Limpiar pantalla anterior
            screen.blit(fondo,(0,0))  #variable para reemplazar el fondo predeterminado por la imagen elegida

            #Dibujar de nuevo todo
            dibujar(screen, palabraUsuario, lista, puntos,racha2, segundos, ayuda)
            pygame.display.flip()
            if segundos<1:
                pygame.mixer.music.stop()       #Corta la cancion de fondo
                ultimos=open("ultimoPuntaje.txt","w")   #Escribe los puntos en un archivo txt
                ultimos.write((str(puntos)))        #Toma los puntos y los convierte en cadena para escribir en el txt
                ultimos.close()                     #Cierra el archivo
                pantallaFinal(ANCHO,ALTO)           #Pantalla del final

        while 1:

            #Esperar el QUIT del usuario
            for e in pygame.event.get():
                if e.type == QUIT:
                    pygame.quit()

                    return

        archivo.close()



#Programa Principal ejecuta Main
if __name__ == "__main__":
    main()
