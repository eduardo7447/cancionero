#Ventana principal del juego
TAMANNO_LETRA = 25
TAMANNO_LETRA_GRANDE = 40
FPS_inicial = 60
TIEMPO_MAX = 40
N=9 #la cantidad de letras de canciones en la carpeta

ANCHO = 1024
ALTO = 600
COLOR_LETRAS = (255,255,255)
COLOR_FONDO = (0,0,0)
COLOR_TEXTO = (255,255,255)
COLOR_TIEMPO_FINAL = (200,20,10)
COLOR_PELI=(200,20,10)
COLOR_CORRECTA = (195,21,21)



# Ventana de menu
ANCHO = 1024
ALTO = 600

# Colors                    #Variables para cambiar el color de las letras
NEGRO = (0, 0, 0)
VERDE = (0, 255, 0)
ROJO = (255, 0, 0)
AMARILLO = (255, 255, 12)
NARANJA = (255,150,0)
CORAL = (240, 128, 128)