from configuracion import *
from pygame.locals import *
import random,sys
##import math
import unicodedata
import pygame


def draw_text(text, font, surface, x, y, main_color, background_color=None): #Funcion para centrar las palabras que aparecen en el menu
    textobj = font.render(text, True, main_color, background_color)
    textrect = textobj.get_rect()
    textrect.centerx = x
    textrect.centery = y
    surface.blit(textobj, textrect)

def menu(ANCHO,ALTO):
    screen = pygame.display.set_mode((ANCHO, ALTO))

    pygame.display.set_caption('Menu')
    comenzar=True
    while comenzar:
        title_font = pygame.font.Font('freesansbold.ttf', 65)       #Fuente de la letra en el menu
        big_font = pygame.font.Font(None, 36)
        draw_text("Cancionero", title_font, screen,ANCHO / 2, ALTO / 4, ROJO, )    #Titulo del menu
        draw_text("El objetivo es adivinar el artista o la canción lo más rapido posible", big_font, screen, ANCHO / 2, ALTO / 2, VERDE, NEGRO)     #subtitulo del menu
        draw_text("Hacé click para empezar", big_font, screen, ANCHO / 2, ALTO / 1.7, VERDE, NEGRO)     #mensaje del menu
        pygame.display.flip()           #Actualiza la pantalla
        temporizador=pygame.time.get_ticks()/1000               #Inicia un contador de segundos en el menu

        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:     #Toma el click del jugador
                return (temporizador)
            if event.type == QUIT:          #Toma la funcion para cerrar la pantalla con la X de la ventana
                pygame.quit()
                sys.exit()
                return()



def pantallaFinal(ANCHO,ALTO):            #Funcion que abre la pantalla al finalizar el tiempo
##    default_font = pygame.font.Font(None, 40)       #Fuente de escritura predeterminada
    screen = pygame.display.set_mode((ANCHO, ALTO))     #Setea la pantalla final con resolucion
    pygame.display.set_caption('Menu')
    comenzar=True
    while comenzar:

        title_font = pygame.font.Font('freesansbold.ttf', 65)       #Fuente del titulo en el final
        big_font = pygame.font.Font(None, 40)           #Fuente de subtitulo en el final
        draw_text("Game Over", title_font, screen,ANCHO / 2, ALTO / 4, NARANJA, )    #Titulo
        draw_text("Hacé click para salir", big_font, screen, ANCHO / 2, ALTO / 2, CORAL, NEGRO)     #subtitulo
        draw_text("¡¡Muchas gracias por jugar !!", big_font, screen, ANCHO / 2, ALTO / 1.7, CORAL, NEGRO)   #Subtitulo
        pygame.display.flip()


        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN or event.type== QUIT:      #Cierra la ventana final al hacer click o apretar la X
                pygame.quit()
                sys.exit()
                return


def artistaEN(lista):       #Recibe una cadena de opciones correctas y la transforma en una lista
        paseAcadena=""
        for elem in lista:
            paseAcadena=paseAcadena+elem
        indice3=""
        nueva=""
        cadenanueva=[]
        for elem in paseAcadena:   #Transforma la linea 0 de cada txt en una lista, para luego poder poner como correcto cualquiera de las opciones
            if elem !=";":          #No toma al ; que separa las opciones correctas de la linea 0
                nueva=nueva+elem
            else:
                if elem==";" :
                    cadenanueva.append(nueva)
                    nueva=""
        for letra in range(len(nueva)-1):       #Permite que tome como correctas las 3 opciones
            indice3=indice3+nueva[letra]        #Elimina el \n de la linea 0

        cadenanueva.append(indice3)
        return cadenanueva

def lectura(archivo, letra, artistaYcancion): #Lee las primeras tres lineas de cada txt
    lineas=archivo.readlines()
    for i in range(3):          #Carga las primeras 3 lineas de cada txt #Recordar que la linea 0 son las opciones correctas
        if i ==0:
            artistaYcancion.append(lineas[i])       #Opciones correctas
        else:
            letra.append(lineas[i])         #Lineas de cancion
    print(artistaYcancion)                #ayuda para verificar la cancion en el interprete
    archivo.close()


def seleccion(letra):   #Corrige las lineas de cancion y borra caracteres indeseados
    cambio=letra[0]          # primer linea del txt
    cambioDos=letra[1]      #segunda linea del txt
    pos=len(cambio)-1       #Cuenta los caracteres de la linea 1
    pos2=len(cambioDos)-1   #Cuenta los caracteres de la linea 2
    linea1=""
    lineaDos=""

    for l in range(pos):
        linea1=linea1+cambio[l]     #Saca el caracter salto de linea (\n) del final de la linea 1

    for e in range(pos2):
        lineaDos=lineaDos+cambioDos[e]      ##Saca el caracter salto de linea (\n) del final de la linea 2

    return (linea1,lineaDos)

##def puntos(n):  #Juntamos las funciones puntos y esCorrecta en una misma



def esCorrecta(palabraUsuario, artistaYCancion, correctas):     #Verifica si lo ingresado por el jugador sea correcta o no  #Genera una racha por seguidilla de correctas
    linea0=artistaYCancion[0]
    opciones=artistaEN(linea0)      # crea una lista que en cada indice esten los nombres del artista de la cancion, o como se lo conoce
    # sumatoria de puntos
    if palabraUsuario in opciones:      #Si lo ingresado por el usuario esta en la lista de opciones
        acumulador=1    #Suma un punto al acertar
        if correctas >1: #Arranca en 1 porque el contador de correctas inicia en 0 al empezar el juego
## El if correctas  >1 se debe a que la variable correctas en el archivo principal tiene su inicio en 0, por lo tanto para armar una
##racha que sume 10 pts en la 3er respuesta correcta tomamos desde >1 porque va 2 iteraciones atrasado el contador.

            acumulador=10   #racha de puntos, suma de a 10 despues de 4 correctas hasta fallar
    else:
        acumulador=-1      #Resta un punto al equivocarse
    return acumulador



